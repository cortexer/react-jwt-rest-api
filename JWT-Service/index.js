var _ = require("lodash");
var express = require("express");
var router = require("router");
var bodyParser = require("body-parser");

var passport = require("passport");
var config  = require('./config');
var cors = require('cors');

var mongoose = require('mongoose');
var restful = require('node-restful');
var jwt = require('jsonwebtoken');

var passportLocalMongoose = require('passport-local-mongoose');
var LocalStrategy = require('passport-local').Strategy;

mongoose.connect('mongodb://localhost/blogtest');
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

passport.use('local-login', new LocalStrategy({
    username : 'username',
    password : 'password',
    passReqToCallback : true
},
function(req, username, password,done) {
    User.findOne({ 'local.username' : username}, function(err, user) {

        if(err)
            return done(err);

        if(!user)
            return done(null, false, req.flash('loginMessage', 'No user found.'));

        if(!user.validPassword(password))
            return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

        return done(null, user);
    });
}));

var app = express();
var router = router();
app.use(cors());
app.use(passport.initialize());
app.set('superSecret', config.secret);

// parse application/x-www-form-urlencoded
// for easier testing with Postman or plain HTML forms
app.use(bodyParser.urlencoded({
  extended: true
}));

// parse application/json
app.use(bodyParser.json())

app.get('/', function (req, res) {
    res.sendFile(process.cwd() + '/public/index.html');
});

app.use(express.static('public'));

app.get('/test', (req, res, next) => {
  if (req.isAuthenticated()) {
      //return next();
      res.status(200).send("pong!");
    }
    res.status(200).send("Not auth!");
});


var users = [{}];

var query = Account.find({});
query.limit(1000).exec(function (err, accounts) {
    if (err) throw err;
    //socket.emit('load old msgs', docs);
    accounts.forEach(function(account) {
    users.push(account);
});
});

app.post('/users', function(req, res) {
  if (!req.body.username || !req.body.password) {
    return res.status(400).send("You must send the username and the password");
  }
  if (_.find(users, {username: req.body.username})) {
   return res.status(400).send("A user with that username already exists");
  }

  var profile = _.pick(req.body, 'username', 'password');

  Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
      if (err) {
        return res.json({message: err});
      }
      profile.id = account._id;
      users.push(profile);
      passport.authenticate('local')(req, res, function () {
        	// if user is found and password is right
			// create a token
			var token = jwt.sign(profile, app.get('superSecret'), {
				expiresIn: 86400 // expires in 24 hours
			});

			res.json({
				success: true,
				message: 'Enjoy your token!',
				token: token
			});
      });
  });
	console.log(users);

});

app.post('/sessions/create', passport.authenticate('local'), function(req, res) {
  if (!req.body.username || !req.body.password) {
    return res.status(400).send("You must send the username and the password");
  }
  var user = _.find(users, {username: req.body.username});
	console.log(user);
  if (!user) {
    return res.status(401).send("The username or password don't match");
  }
	// if user is found and password is right
	// create a token
	var token = jwt.sign(user, app.get('superSecret'), {
		expiresIn: 86400 // expires in 24 hours
	});

	res.json({
		success: true,
		message: 'Enjoy your token!',
		token: token
	});
});

function checkToken(req, res, next) {
	// check header or url parameters or post parameters for token
	var token = req.body.token || req.param('token') || req.headers['x-access-token'];
	// decode token
	if (token) {
		console.log("req is: ");
		console.log(req.body);
		// verifies secret and checks exp
		jwt.verify(token, app.get('superSecret'), function(err, decoded) {			
			if (err) {
				return res.json({ success: false, message: 'Failed to authenticate token.' });		
			} else {
				// if everything is good, save to request for use in other routes
				req.decoded = decoded;	
				return req, res, next();
			}
		});
	} else {
		// if there is no token
		// return an error
		return res.status(403).send({
			success: false, 
			message: 'No token provided.'
		});		
}
}

app.get('/items', function(req, res, next) {
    checkToken(req, res, next)
});
app.post('/items', function(req, res, next) {
    checkToken(req, res, next)
});
app.put('/items', function(req, res, next) {
    checkToken(req, res, next)
});
app.delete('/items', function(req, res, next) {
    checkToken(req, res, next)
});

var ItemSchema = mongoose.Schema({
    name: String,
	description: String,
	imgurl: String,
	imgurl2: String,
	imgurl3: String,
    startingPrice: Number,
    price: Number,
	addedBy: String
});

var item = restful.model('item', ItemSchema);
item.methods(['get', 'post', 'put', 'delete']);
item.register(app, '/items');

app.listen(3000, function() {
  console.log("Express running");
});
